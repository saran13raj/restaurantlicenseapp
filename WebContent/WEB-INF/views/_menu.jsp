<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Menu</title>
</head>
<body><center>
<a href="${pageContext.request.contextPath}/AdminTaskServlet">
  Admin Task
</a>||
<a href="${pageContext.request.contextPath}/AgentTaskServlet">
  Agent Task
</a>


||
<a href="${pageContext.request.contextPath}/UserInfoServlet">
  User Info
</a>       
||
<a href="${pageContext.request.contextPath}/SearchRestaurantData">
  Search Restaurant
</a>
||
<a href="${pageContext.request.contextPath}/LogoutServlet">
  Logout
</a>
 
&nbsp;
<span style="color:red">[ ${loginedUser.userName} ]</span>
</center>
</body>
</html>