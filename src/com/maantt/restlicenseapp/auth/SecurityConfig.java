package com.maantt.restlicenseapp.auth;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class SecurityConfig {
	 public static final String ROLE_ADMIN = "ADMIN";
	    public static final String ROLE_AGENT = "AGENT";
	 
	    // String: Role
	    // List<String>: urlPatterns.
	    private static final Map<String, List<String>> mapConfig = new HashMap<String, List<String>>();
	 
	    static {
	        init();
	    }
	 
	    private static void init() {
	 
	        // Configure For "Agent" Role.
	        List<String> urlPatterns1 = new ArrayList<String>();
	 
	        urlPatterns1.add("/UserInfoServlet");
	        urlPatterns1.add("/AgentTaskServlet");
	        urlPatterns1.add("/UpdateRestaurantDataServlet");
	        urlPatterns1.add("/DeleteRestaurantDataServlet");
	 
	        mapConfig.put(ROLE_AGENT, urlPatterns1);
	 
	        // Configure For "Admin" Role.
	        List<String> urlPatterns2 = new ArrayList<String>();
	 
	        urlPatterns2.add("/UserInfoServlet");
	        urlPatterns2.add("/AdminTaskServlet");
	        urlPatterns2.add("/FileUploadServlet");
	        urlPatterns2.add("/AddAgentServlet");
	        urlPatterns2.add("/UpdateRestaurantDataServlet");
	        urlPatterns2.add("/DeleteRestaurantDataServlet");
	        
	        
	        mapConfig.put(ROLE_ADMIN, urlPatterns2);
	    }
	 
	    public static Set<String> getAllAppRoles() {
	        return mapConfig.keySet();
	    }
	 
	    public static List<String> getUrlPatternsForRole(String role) {
	        return mapConfig.get(role);
	    }

}
